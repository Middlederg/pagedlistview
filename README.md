**PagedListView**

Control Personalizado que incluye ListView paginado. Permite filtrado para búsquedas, y añadir cualquier control (Textbox, Button, etc...) en la columna que se desee.

*.NET Framework 4.5*

![ejemplo1](PagedList/Img/example1.png)


---

## Como utilizar la librería

Agregar la siguiente referencia en el proyecto:

1. PagedList.dll

Adicionalmente, es conveniente instalar el paquete Nuget para compatibilidad con ValueTuple *System.ValueTuple*

---

## Ejemplo de utilización

---

```csharp

//Genero una lista de personas a mostrar
var personas = Data.ListaPersonas.Take(35);

//Añado las columnas
Plvw.AddColumn("Nombre", 250);
Plvw.AddColumn("Edad", 70);
Plvw.AddColumn("Género", 70);
Plvw.AddColumn("Procedencia", 280);

//Creo una lista de imagenes para el listView (Opcional)
var imgs = new List<Image>()
{
	IconChar.UserAstronaut.ToBitmap(30, Color.Black),
	IconChar.Burn.ToBitmap(30, Color.Black)
};
Plvw.Lvw.SetImagenes(30, 30, imgs);

//Para cada fila
foreach (var p in personas)
{
	//Creo el ListViewItem
	var item = ItemHelper.Create(new List<string>() { p.NombreCompleto, p.Edad.ToString(), "", p.Origen.ToString() }, 1);

    //Creo Lista de Controles con su correspondiente posición
    var controles = new List<(Control control, int columna)>()
    {
      	(ButtonHelper.CreateIcon(1, ClickButton, IconChar.User), 2)
	};
    Plvw.Lvw.AddItem(item, controles);
}

//Cambio las opciones
Plvw.Options = new Model.PagedListViewOptions()
{
	ChooseRecordNumber = true,
  	Filter = true,
	FilterInstanctly = true,
	Footer = true,
	FullRowSelect = true,
	HideSelection = false,
	MultiSelect = false,
	ShowGridLines = true
};

//Selecciono el número de registros que quiero por página
Plvw.CambiarRegistrosPorPagina(10);

//Cargo la primera página
Plvw.CambiarDePagina(1);

```
