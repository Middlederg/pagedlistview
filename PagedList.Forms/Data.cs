﻿using FontAwesome.Sharp;
using PagedList.Helpers;
using Personas.Core;
using Personas.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PagedList.Forms
{
    public static class Data
    {
        public static List<Persona> ListaPersonas;

        private static readonly Dal dal;
        static Data()
        {
            dal = new Dal(2018);
            ListaPersonas = dal.GetPersonas(1200);
        }

        public static ListViewItem GetItem(Persona p)
        {
            return ItemHelper.Create(new List<string>() { p.NombreCompleto, p.Edad.ToString(), "", p.Origen.ToString() });
        }

        public static IEnumerable<(Control control, int columna)> GetControls(Persona p)
        {
            yield return (ButtonHelper.CreateIcon(1, Click, IconChar.User), 2);
        }

        private static void Click(object sender, EventArgs args) => MessageBox.Show("Click");

    }
}
