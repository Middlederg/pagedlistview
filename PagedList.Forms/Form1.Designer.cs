﻿namespace PagedList.Forms
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.Plvw = new PagedList.UserControls.PagedListView();
            this.SuspendLayout();
            // 
            // Plvw
            // 
            this.Plvw.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Plvw.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Plvw.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Plvw.Location = new System.Drawing.Point(0, 0);
            this.Plvw.Margin = new System.Windows.Forms.Padding(0);
            this.Plvw.Name = "Plvw";
            this.Plvw.Size = new System.Drawing.Size(800, 450);
            this.Plvw.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Plvw);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private UserControls.PagedListView Plvw;
    }
}

