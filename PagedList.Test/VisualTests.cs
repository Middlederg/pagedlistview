﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using FontAwesome.Sharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PagedList.CustomControls;
using PagedList.Helpers;
using Personas.Core.Model;

namespace PagedList.Test
{
    [TestClass]
    public class VisualTests
    {

        [TestMethod]
        public void LvExtMostrarPersonas15()
        {
            var personas = Data.ListaPersonas.Take(15);

            Form f = new Form();
            ExtendedListView lvw = new ExtendedListView() { Dock = DockStyle.Fill};
            lvw.Columns.Add("Nombre", 250);
            lvw.Columns.Add("Edad", 70);
            lvw.Columns.Add("Género", 70);
            lvw.Columns.Add("Procedencia", 180);
            foreach (var p in personas)
            {
                lvw.AddItem(Data.GetItem(p), Data.GetControls(p).ToList());
            }
            lvw.Renderizar();
            f.Controls.Add(lvw);
            f.ShowDialog();
        }
    }
}
