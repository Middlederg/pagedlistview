﻿using System;
using System.Linq;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PagedList.CustomControls;
using PagedList.UserControls;

namespace PagedList.Test.PagedListTest
{
    [TestClass]
    public class PagedListVisual
    {
        [TestMethod]
        public void MostrarPersonas15()
        {
            var personas = Data.ListaPersonas.Take(15);

            Form f = new Form();
            PagedListView p = new PagedListView() { Dock = DockStyle.Fill };
            p.AddColumn("Nombre", 250);
            p.AddColumn("Edad", 70);
            p.AddColumn("Género", 70);
            p.AddColumn("Procedencia", 280);
            foreach (var pers in personas)
            {
                p.Lvw.AddItem(Data.GetItem(pers), Data.GetControls(pers).ToList());
            }
            p.Lvw.Renderizar();
            f.Controls.Add(p);
            f.ShowDialog();
        }
    }
}
