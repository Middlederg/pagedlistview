﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace PagedList.Model
{
    public class ItemLista
    {
        public int Indx { get; set; }
        public ListViewItem Lvwitem { get; set; }
        public bool Visible { get; set; }
        public List<(Control control, int columna)> Controles { get; set; }

        public override bool Equals(object obj) => Lvwitem.Equals(obj as ListViewItem);
        public override int GetHashCode() => Lvwitem.GetHashCode();
    }
}
