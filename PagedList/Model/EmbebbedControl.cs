﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PagedList.Model
{
    public class EmbeddedControl
    {
        public Control Control { get; set; }
        public int Column { get; set; }
        public int Row { get; set; }
        public DockStyle Dock { get; set; }
        public ListViewItem Item { get; set; }

        public override bool Equals(object obj) => Control.Equals(obj);
        public override int GetHashCode() => Control.GetHashCode();
    }
}
