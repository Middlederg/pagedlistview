﻿namespace PagedList.Model
{
    public struct PagedListViewOptions
    {
        /// <summary>
        /// Determina si se muestra la caja de búsqueda
        /// </summary>
        public bool Filter { get; set; }

        /// <summary>
        /// Determina si la búsqueda actúa instantaneamente o solo cuando se pulsa enter
        /// </summary>
        public bool FilterInstanctly { get; set; }

        /// <summary>
        /// Determina si se muestra el Número de registros mostrados para editarlo
        /// </summary>
        public bool ChooseRecordNumber { get; set; }

        /// <summary>
        /// Determina si se muestra el pie que resume el número de registros mostrados y la página actual / total
        /// </summary>
        public bool Footer { get; set; }

        /// <summary>
        /// Muestra rejilla en el listview
        /// </summary>
        public bool ShowGridLines { get; set; }

        /// <summary>
        /// Al seleccionar una fila, se selecciona completamente
        /// </summary>
        public bool FullRowSelect { get; set; }

        /// <summary>
        /// Posibilidad de seleccionar múltiples filas
        /// </summary>
        public bool MultiSelect { get; set; }

        /// <summary>
        /// Oculta la fila seleccionada cuando el control pierde el foco
        /// </summary>
        public bool HideSelection { get; set; }

        public PagedListViewOptions(bool filter = true, bool filterInstanctly = false, bool chooseRecordNumber = true, bool footer = true,
             bool showGridLines = false, bool fullRowSelect = false, bool multiSelect = false, bool hideSelection = true)
        {
            Filter = filter;
            FilterInstanctly = filterInstanctly;
            ChooseRecordNumber = chooseRecordNumber;
            Footer = footer;
            ShowGridLines = showGridLines;
            FullRowSelect = fullRowSelect;
            MultiSelect = multiSelect;
            HideSelection = hideSelection;
        }
    }
}
