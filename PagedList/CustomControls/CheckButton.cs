﻿using FontAwesome.Sharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PagedList.CustomControls
{
    public partial class CheckButton<T> : IconButton
    {
        public T Objeto { get; set; }
        public IconChar CheckedImage { get; set; } = IconChar.Check;
        public IconChar UnCheckedImage { get; set; } = IconChar.None;

        private bool check;
        public bool Checked
        {
            get { return check; }
            set
            {
                check = value;
                IconChar = (check) ? CheckedImage : UnCheckedImage;
            }
        }

        public CheckButton()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        private void ClickButton(object sender, EventArgs e) => Checked = !check;
        
    }
}
