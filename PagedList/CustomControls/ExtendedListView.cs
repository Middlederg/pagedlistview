﻿using PagedList.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PagedList.CustomControls
{
    public partial class ExtendedListView : ListView
    {
        #region Interop-Defines
        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wPar, IntPtr lPar);

        // ListView messages
        private const int LVM_FIRST = 0x1000;
        private const int LVM_GETCOLUMNORDERARRAY = (LVM_FIRST + 59);

        // Windows Messages
        private const int WM_PAINT = 0x000F;
        #endregion

        public ExtendedListView()
        {
            InitializeComponent();
        }

        private List<EmbeddedControl> _embeddedControls = new List<EmbeddedControl>();
        private List<ItemLista> _listViewItems = new List<ItemLista>();
        public void Clean()
        {
            Controls.Clear();
            _embeddedControls.Clear();
            Items.Clear();
        }

        /// <summary>
        /// Retrieve the order in which columns appear
        /// </summary>
        /// <returns>Current display order of column indices</returns>
        protected int[] GetColumnOrder()
        {
            IntPtr lPar = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(int)) * Columns.Count);

            IntPtr res = SendMessage(Handle, LVM_GETCOLUMNORDERARRAY, new IntPtr(Columns.Count), lPar);
            if (res.ToInt32() == 0) // Something went wrong
            {
                Marshal.FreeHGlobal(lPar);
                return null;
            }

            int[] order = new int[Columns.Count];
            Marshal.Copy(lPar, order, 0, Columns.Count);

            Marshal.FreeHGlobal(lPar);

            return order;
        }

        /// <summary>
        /// Retrieve the bounds of a ListViewSubItem
        /// </summary>
        /// <param name="Item">The Item containing the SubItem</param>
        /// <param name="SubItem">Index of the SubItem</param>
        /// <returns>Subitem's bounds</returns>
        protected Rectangle GetSubItemBounds(ListViewItem Item, int SubItem)
        {
            Rectangle subItemRect = Rectangle.Empty;

            if (Item == null)
                throw new ArgumentNullException("Item");

            int[] order = GetColumnOrder();
            if (order == null) // No Columns
                return subItemRect;

            if (SubItem >= order.Length)
                throw new IndexOutOfRangeException("SubItem " + SubItem + " out of range");

            // Retrieve the bounds of the entire ListViewItem (all subitems)
            Rectangle lviBounds = Item.GetBounds(ItemBoundsPortion.Entire);
            int subItemX = lviBounds.Left;

            // Calculate the X position of the SubItem.
            // Because the columns can be reordered we have to use Columns[order[i]] instead of Columns[i] !
            ColumnHeader col;
            int i;
            for (i = 0; i < order.Length; i++)
            {
                col = this.Columns[order[i]];
                if (col.Index == SubItem)
                    break;
                subItemX += col.Width;
            }

            subItemRect = new Rectangle(subItemX, lviBounds.Top, this.Columns[order[i]].Width, lviBounds.Height);

            return subItemRect;
        }

        /// <summary>
        /// Add a control to the ListView
        /// </summary>
        /// <param name="c">Control to be added</param>
        /// <param name="col">Index of column</param>
        /// <param name="row">Index of row</param>
        /// <param name="dock">Location and resize behavior of embedded control</param>
        public void AddEmbeddedControl(Control c, int col, int row, DockStyle dock = DockStyle.Fill)
        {
            if (c == null)
                throw new ArgumentNullException();
            if (col >= Columns.Count || row >= Items.Count)
                throw new ArgumentOutOfRangeException();

            // Add a Click event handler to select the ListView row when an embedded control is clicked
            c.Click += new EventHandler(_embeddedControl_Click);

            EmbeddedControl ec = new EmbeddedControl()
            {
                Control = c,
                Column = col,
                Row = row,
                Dock = dock,
                Item = Items[row]
            };
            _embeddedControls.Add(ec);
            Controls.Add(c);
        }

        /// <summary>
        /// Remove a control from the ListView
        /// </summary>
        /// <param name="c">Control to be removed</param>
        public void RemoveEmbeddedControl(Control c)
        {
            if (c == null)
                throw new ArgumentNullException();

            for (int i = 0; i < _embeddedControls.Count; i++)
            {
                EmbeddedControl ec = (EmbeddedControl)_embeddedControls[i];
                if (ec.Control == c)
                {
                    c.Click -= new EventHandler(_embeddedControl_Click);
                    this.Controls.Remove(c);
                    _embeddedControls.RemoveAt(i);
                    return;
                }
            }
            throw new Exception("Control not found!");
        }

        /// <summary>
        /// Retrieve the control embedded at a given location
        /// </summary>
        /// <param name="col">Index of Column</param>
        /// <param name="row">Index of Row</param>
        /// <returns>Control found at given location or null if none assigned.</returns>
        public Control GetEmbeddedControl(int col, int row)
        {
            foreach (EmbeddedControl ec in _embeddedControls)
                if (ec.Row == row && ec.Column == col)
                    return ec.Control;

            return null;
        }

        /// <summary>
        /// Retrieve the row where a control is located
        /// </summary>
        /// <param name="c"></param>
        /// <returns>Row where the control is located in the listview</returns>
        public int? GetEmbeddedControlsRow(Control c)
        {
            foreach (EmbeddedControl ec in _embeddedControls)
                if (ec.Equals(c))
                    return ec.Row;

            return null;
        }

        /// <summary>
        /// Deja visibles los elementos indicados por el filtro
        /// </summary>
        /// <param name="filtro"></param>
        public void Filtrar(string filtro)
        {
            foreach (var x in _listViewItems) x.Visible = true;
            if (!string.IsNullOrWhiteSpace(filtro))
            {
                foreach (var item in _listViewItems)
                    item.Visible = item.Lvwitem.Text.ContieneTexto(filtro.Trim());
            }
        }

        /// <summary>
        /// Deja visibles los elementos de la página actual
        /// </summary>
        /// <param name="paginaActual"></param>
        /// <param name="RegistrosPorPagina"></param>
        /// <param name="filtro"></param>
        public void AplicarPaginacion(int paginaActual, int RegistrosPorPagina, string filtro = "")
        {
            int registroInicial = (paginaActual - 1) * RegistrosPorPagina;
            int registroFinal = (paginaActual) * RegistrosPorPagina;
            int pos = 0;
            foreach (var item in _listViewItems.Where(x => x.Visible))
            {
                item.Visible = pos >= registroInicial && pos < registroFinal;
                pos++;
            }
        }

        /// <summary>
        /// Agrega al listview todos los elementos visibles
        /// </summary>
        public void Renderizar()
        {
            Clean();
            int iRow = 0;
            foreach (var item in _listViewItems.Where(x => x.Visible))
            {
                Items.Add(item.Lvwitem);
                foreach (var (control, columna) in item.Controles)
                    AddEmbeddedControl(control, columna, iRow);
                iRow++;
            }
        }

        /// <summary>
        /// Total de elementos visibles
        /// </summary>
        /// <returns></returns>
        public int ElementosTotales() => _listViewItems.Count(x => x.Visible);

        public void AddItem(ListViewItem item, List<(Control control, int columna)> controles, int? imgIndx = null)
        {
            var max = _listViewItems.Any() ? _listViewItems.Max(x => x.Indx) : -1;
            if (imgIndx.HasValue)
                item.ImageIndex = imgIndx.Value;
            _listViewItems.Add(new ItemLista() { Indx = max + 1, Lvwitem = item, Visible = true, Controles = controles });
        }

        public void SetImagenes(int height = 30, int width = 1, IEnumerable<Image> imagenes = null)
        {
            ImageList imgList = new ImageList();
            if (imagenes != null)
                imgList = ImageLists.CreateImageList(imagenes);
            
            imgList.ImageSize = new Size(width, height);
            SmallImageList = imgList;
            //LargeImageList = imgList;
        }

        [DefaultValue(View.LargeIcon)]
        public new View View
        {
            get
            {
                return base.View;
            }
            set
            {
                // Embedded controls are rendered only when we're in Details mode
                foreach (EmbeddedControl ec in _embeddedControls)
                    ec.Control.Visible = (value == View.Details);

                base.View = value;
            }
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_PAINT:
                    if (View != View.Details)
                        break;

                    // Calculate the position of all embedded controls
                    foreach (EmbeddedControl ec in _embeddedControls)
                    {
                        Rectangle rc = this.GetSubItemBounds(ec.Item, ec.Column);

                        if ((this.HeaderStyle != ColumnHeaderStyle.None) &&
                            (rc.Top < this.Font.Height)) // Control overlaps ColumnHeader
                        {
                            ec.Control.Visible = false;
                            continue;
                        }
                        else
                        {
                            ec.Control.Visible = true;
                        }

                        switch (ec.Dock)
                        {
                            case DockStyle.Fill:
                                rc.Y = rc.Y + ec.Control.Margin.Top;
                                rc.X = rc.X + ec.Control.Margin.Left;
                                rc.Width = rc.Width - ec.Control.Margin.Left - ec.Control.Margin.Right;
                                rc.Height = rc.Height - ec.Control.Margin.Top - ec.Control.Margin.Bottom;
                                break;
                            case DockStyle.Top:
                                rc.Height = ec.Control.Height;
                                break;
                            case DockStyle.Left:
                                rc.Width = ec.Control.Width;
                                break;
                            case DockStyle.Bottom:
                                rc.Offset(0, rc.Height - ec.Control.Height);
                                rc.Height = ec.Control.Height;
                                break;
                            case DockStyle.Right:
                                rc.Offset(rc.Width - ec.Control.Width, 0);
                                rc.Width = ec.Control.Width;
                                break;
                            case DockStyle.None:
                                rc.Size = ec.Control.Size;
                                break;
                        }

                        // Set embedded control's bounds
                        ec.Control.Bounds = rc;
                    }
                    break;
            }
            base.WndProc(ref m);
        }

        private void _embeddedControl_Click(object sender, EventArgs e)
        {
            // When a control is clicked the ListViewItem holding it is selected
            foreach (EmbeddedControl ec in _embeddedControls)
            {
                if (ec.Control == (Control)sender)
                {
                    this.SelectedItems.Clear();
                    ec.Item.Selected = true;
                }
            }
        }
    }
    }
