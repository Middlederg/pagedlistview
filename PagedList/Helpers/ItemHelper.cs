﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace PagedList.Helpers
{
    public static class ItemHelper
    {
        public static ListViewItem Create(List<string> elementos, int? imgIndx = null, ListViewGroup grupo = null)
        {
            if (elementos == null || elementos.Count < 1)
                throw new ArgumentNullException(nameof(elementos));
            
            var item = new ListViewItem(elementos.First());
            if (imgIndx.HasValue) item.ImageIndex = imgIndx.Value;
            if (grupo != null) item.Group = grupo;
            foreach (string e in elementos.Skip(1))
                item.SubItems.Add(e);
            return item;
        }

        public static ListViewGroup CreateGroup(string texto) => new ListViewGroup(texto);
    }
}
