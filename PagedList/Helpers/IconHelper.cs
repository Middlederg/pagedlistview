﻿using FontAwesome.Sharp;
using System;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PagedList.Helpers
{
    public static class ComboHelper
    {
        public static ComboBox Create(EventHandler onChanged)
        {
            ComboBox cmb = new ComboBox()
            {
                Margin = new Padding(0),
                DropDownStyle = ComboBoxStyle.DropDownList,
                Width = 30
            };
            if (onChanged != null)
                cmb.SelectedIndexChanged += onChanged;
            return cmb;
        }
    }
    public static class ButtonHelper
    {
        public static Button Create(int id, EventHandler onclick, string text)
        {
            Button btn = new Button()
            {
                Margin = new Padding(0),
                FlatStyle = FlatStyle.Flat,
                Width = 30,
                Tag = id,
                Text = text
            };
            btn.FlatAppearance.BorderSize = 0;
            if (onclick != null)
                btn.Click += onclick;
            return btn;
        }

        public static IconButton CreateIcon(int id, EventHandler onclick, IconChar icon = IconChar.None, int size = 18)
        {
            IconButton btn = new IconButton()
            {
                IconChar = icon,
                IconSize = size,
                Margin = new Padding(0),
                FlatStyle = FlatStyle.Flat,
                Width = 30,
                Tag = id
            };
            btn.FlatAppearance.BorderSize = 0;
            if (onclick != null)
                btn.Click += onclick;
            return btn;
        }
    }
}
