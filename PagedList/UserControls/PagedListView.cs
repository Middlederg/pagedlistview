﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PagedList.Model;

namespace PagedList.UserControls
{
    public partial class PagedListView : UserControl
    {
        public PagedListViewOptions Options { get; set; }
        public int RegistrosPorPagina { get { return (int)NudRegistrosPorPagina.Value; } }

        private int pagina = 1;
        private int ultimoCalculoTotalPaginas;

        /// <summary>
        /// Número de páginas que tiene el ListView
        /// </summary>
        private int NumPaginasTotales
        {
            get
            {
                int res = (int)Math.Truncate((decimal)(Lvw.ElementosTotales() / RegistrosPorPagina));
                return res + (Lvw.ElementosTotales() % RegistrosPorPagina == 0 ? 0 : 1);
            }
        }

        public PagedListView()
        {
            InitializeComponent();
            NudRegistrosPorPagina.Value = RegistrosPorPagina;
        }

        private void BtnFirst_Click(object sender, EventArgs e) => CambiarDePagina(1);
        private void BtnPreviuos_Click(object sender, EventArgs e) => CambiarDePagina(pagina - 1);
        private void BtnNext_Click(object sender, EventArgs e) => CambiarDePagina(pagina + 1);
        private void BtnLast_Click(object sender, EventArgs e) => CambiarDePagina(ultimoCalculoTotalPaginas);

        public void CambiarRegistrosPorPagina(int num) => NudRegistrosPorPagina.Value = num;

        public void CambiarDePagina(int pagDestino)
        {
            pagina = pagDestino;
            Lvw.Filtrar(TbxBusqueda.Text.Trim());
            Botones();
            Lvw.AplicarPaginacion(pagina, RegistrosPorPagina);
            Lvw.Renderizar();
            CambiarOpciones();
        }

        public void CambiarOpciones()
        {
            NudRegistrosPorPagina.Visible = LblRegistrosPorPagina.Visible = Options.ChooseRecordNumber;
            TbxBusqueda.Visible = LblTituloBusqueda.Visible = Options.Filter;
            LblPagina.Visible = Options.Footer;
            TlpPrincipal.RowStyles[0].Height = Options.ChooseRecordNumber || Options.Filter ? 30 : 0;
            Lvw.GridLines = Options.ShowGridLines;
            Lvw.HideSelection = Options.HideSelection;
            Lvw.FullRowSelect = Options.FullRowSelect;
            Lvw.MultiSelect = Options.MultiSelect;
        }

        public void AddColumn(string nombre, int ancho = 100) => Lvw.Columns.Add(nombre, ancho);
        
        private void Botones()
        {
            BtnFirst.Enabled = pagina != 1;
            BtnLast.Enabled = pagina != NumPaginasTotales;
            BtnPreviuos.Enabled = BtnFirst.Enabled;
            BtnNext.Enabled = BtnLast.Enabled;

            LblPagina.Text = $"Página {pagina}/{NumPaginasTotales}. {Lvw.ElementosTotales()} registros";
            ultimoCalculoTotalPaginas = NumPaginasTotales;
        }

        private void NudRegistrosPorPagina_ValueChanged(object sender, EventArgs e) => CambiarDePagina(1);

        private void TbxBusqueda_TextChanged(object sender, EventArgs e)
        {
            if (Options.FilterInstanctly)
                CambiarDePagina(1);
        }

        private void BuscarKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
                CambiarDePagina(1);
        }
    }
}
