﻿namespace PagedList.UserControls
{
    partial class PagedListView
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.TlpPrincipal = new System.Windows.Forms.TableLayoutPanel();
            this.FlpBotones = new System.Windows.Forms.FlowLayoutPanel();
            this.BtnLast = new FontAwesome.Sharp.IconButton();
            this.BtnNext = new FontAwesome.Sharp.IconButton();
            this.BtnPreviuos = new FontAwesome.Sharp.IconButton();
            this.BtnFirst = new FontAwesome.Sharp.IconButton();
            this.LblRegistrosPorPagina = new System.Windows.Forms.Label();
            this.LblTituloBusqueda = new System.Windows.Forms.Label();
            this.TbxBusqueda = new System.Windows.Forms.TextBox();
            this.NudRegistrosPorPagina = new System.Windows.Forms.NumericUpDown();
            this.Lvw = new PagedList.CustomControls.ExtendedListView();
            this.LblPagina = new System.Windows.Forms.Label();
            this.TlpPrincipal.SuspendLayout();
            this.FlpBotones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NudRegistrosPorPagina)).BeginInit();
            this.SuspendLayout();
            // 
            // TlpPrincipal
            // 
            this.TlpPrincipal.ColumnCount = 4;
            this.TlpPrincipal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.TlpPrincipal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TlpPrincipal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TlpPrincipal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.TlpPrincipal.Controls.Add(this.FlpBotones, 2, 2);
            this.TlpPrincipal.Controls.Add(this.LblRegistrosPorPagina, 2, 0);
            this.TlpPrincipal.Controls.Add(this.LblTituloBusqueda, 0, 0);
            this.TlpPrincipal.Controls.Add(this.TbxBusqueda, 1, 0);
            this.TlpPrincipal.Controls.Add(this.NudRegistrosPorPagina, 3, 0);
            this.TlpPrincipal.Controls.Add(this.Lvw, 0, 1);
            this.TlpPrincipal.Controls.Add(this.LblPagina, 0, 2);
            this.TlpPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TlpPrincipal.Location = new System.Drawing.Point(0, 0);
            this.TlpPrincipal.Margin = new System.Windows.Forms.Padding(0);
            this.TlpPrincipal.Name = "TlpPrincipal";
            this.TlpPrincipal.Padding = new System.Windows.Forms.Padding(5, 10, 5, 10);
            this.TlpPrincipal.RowCount = 3;
            this.TlpPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.TlpPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TlpPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.TlpPrincipal.Size = new System.Drawing.Size(417, 296);
            this.TlpPrincipal.TabIndex = 0;
            // 
            // FlpBotones
            // 
            this.TlpPrincipal.SetColumnSpan(this.FlpBotones, 2);
            this.FlpBotones.Controls.Add(this.BtnLast);
            this.FlpBotones.Controls.Add(this.BtnNext);
            this.FlpBotones.Controls.Add(this.BtnPreviuos);
            this.FlpBotones.Controls.Add(this.BtnFirst);
            this.FlpBotones.Dock = System.Windows.Forms.DockStyle.Right;
            this.FlpBotones.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.FlpBotones.Location = new System.Drawing.Point(208, 256);
            this.FlpBotones.Margin = new System.Windows.Forms.Padding(0);
            this.FlpBotones.Name = "FlpBotones";
            this.FlpBotones.Size = new System.Drawing.Size(204, 30);
            this.FlpBotones.TabIndex = 11;
            // 
            // BtnLast
            // 
            this.BtnLast.FlatAppearance.BorderSize = 0;
            this.BtnLast.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnLast.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.BtnLast.IconChar = FontAwesome.Sharp.IconChar.StepForward;
            this.BtnLast.IconColor = System.Drawing.Color.Black;
            this.BtnLast.IconSize = 25;
            this.BtnLast.Location = new System.Drawing.Point(174, 0);
            this.BtnLast.Margin = new System.Windows.Forms.Padding(0);
            this.BtnLast.Name = "BtnLast";
            this.BtnLast.Rotation = 0D;
            this.BtnLast.Size = new System.Drawing.Size(30, 30);
            this.BtnLast.TabIndex = 8;
            this.BtnLast.UseVisualStyleBackColor = true;
            this.BtnLast.Click += new System.EventHandler(this.BtnLast_Click);
            // 
            // BtnNext
            // 
            this.BtnNext.FlatAppearance.BorderSize = 0;
            this.BtnNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnNext.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.BtnNext.IconChar = FontAwesome.Sharp.IconChar.CaretRight;
            this.BtnNext.IconColor = System.Drawing.Color.Black;
            this.BtnNext.IconSize = 30;
            this.BtnNext.Location = new System.Drawing.Point(139, 0);
            this.BtnNext.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.BtnNext.Name = "BtnNext";
            this.BtnNext.Rotation = 0D;
            this.BtnNext.Size = new System.Drawing.Size(30, 30);
            this.BtnNext.TabIndex = 14;
            this.BtnNext.UseVisualStyleBackColor = true;
            this.BtnNext.Click += new System.EventHandler(this.BtnNext_Click);
            // 
            // BtnPreviuos
            // 
            this.BtnPreviuos.FlatAppearance.BorderSize = 0;
            this.BtnPreviuos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnPreviuos.Flip = FontAwesome.Sharp.FlipOrientation.Horizontal;
            this.BtnPreviuos.IconChar = FontAwesome.Sharp.IconChar.CaretRight;
            this.BtnPreviuos.IconColor = System.Drawing.Color.Black;
            this.BtnPreviuos.IconSize = 30;
            this.BtnPreviuos.Location = new System.Drawing.Point(104, 0);
            this.BtnPreviuos.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.BtnPreviuos.Name = "BtnPreviuos";
            this.BtnPreviuos.Rotation = 0D;
            this.BtnPreviuos.Size = new System.Drawing.Size(30, 30);
            this.BtnPreviuos.TabIndex = 13;
            this.BtnPreviuos.UseVisualStyleBackColor = true;
            this.BtnPreviuos.Click += new System.EventHandler(this.BtnPreviuos_Click);
            // 
            // BtnFirst
            // 
            this.BtnFirst.FlatAppearance.BorderSize = 0;
            this.BtnFirst.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnFirst.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.BtnFirst.IconChar = FontAwesome.Sharp.IconChar.StepBackward;
            this.BtnFirst.IconColor = System.Drawing.Color.Black;
            this.BtnFirst.IconSize = 25;
            this.BtnFirst.Location = new System.Drawing.Point(69, 0);
            this.BtnFirst.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.BtnFirst.Name = "BtnFirst";
            this.BtnFirst.Rotation = 0D;
            this.BtnFirst.Size = new System.Drawing.Size(30, 30);
            this.BtnFirst.TabIndex = 12;
            this.BtnFirst.UseVisualStyleBackColor = true;
            this.BtnFirst.Click += new System.EventHandler(this.BtnFirst_Click);
            // 
            // LblRegistrosPorPagina
            // 
            this.LblRegistrosPorPagina.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LblRegistrosPorPagina.Location = new System.Drawing.Point(211, 15);
            this.LblRegistrosPorPagina.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.LblRegistrosPorPagina.Name = "LblRegistrosPorPagina";
            this.LblRegistrosPorPagina.Size = new System.Drawing.Size(137, 25);
            this.LblRegistrosPorPagina.TabIndex = 3;
            this.LblRegistrosPorPagina.Text = "Registros:";
            this.LblRegistrosPorPagina.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // LblTituloBusqueda
            // 
            this.LblTituloBusqueda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LblTituloBusqueda.Location = new System.Drawing.Point(8, 15);
            this.LblTituloBusqueda.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.LblTituloBusqueda.Name = "LblTituloBusqueda";
            this.LblTituloBusqueda.Size = new System.Drawing.Size(54, 25);
            this.LblTituloBusqueda.TabIndex = 0;
            this.LblTituloBusqueda.Text = "Buscar:";
            // 
            // TbxBusqueda
            // 
            this.TbxBusqueda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TbxBusqueda.Location = new System.Drawing.Point(68, 13);
            this.TbxBusqueda.MaxLength = 100;
            this.TbxBusqueda.Name = "TbxBusqueda";
            this.TbxBusqueda.Size = new System.Drawing.Size(137, 21);
            this.TbxBusqueda.TabIndex = 1;
            this.TbxBusqueda.TextChanged += new System.EventHandler(this.TbxBusqueda_TextChanged);
            this.TbxBusqueda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.BuscarKeyPress);
            // 
            // NudRegistrosPorPagina
            // 
            this.NudRegistrosPorPagina.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NudRegistrosPorPagina.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NudRegistrosPorPagina.Location = new System.Drawing.Point(351, 12);
            this.NudRegistrosPorPagina.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.NudRegistrosPorPagina.Name = "NudRegistrosPorPagina";
            this.NudRegistrosPorPagina.Size = new System.Drawing.Size(61, 23);
            this.NudRegistrosPorPagina.TabIndex = 2;
            this.NudRegistrosPorPagina.ValueChanged += new System.EventHandler(this.NudRegistrosPorPagina_ValueChanged);
            // 
            // Lvw
            // 
            this.TlpPrincipal.SetColumnSpan(this.Lvw, 4);
            this.Lvw.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Lvw.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lvw.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.Lvw.Location = new System.Drawing.Point(5, 40);
            this.Lvw.Margin = new System.Windows.Forms.Padding(0);
            this.Lvw.Name = "Lvw";
            this.Lvw.Size = new System.Drawing.Size(407, 216);
            this.Lvw.TabIndex = 4;
            this.Lvw.UseCompatibleStateImageBehavior = false;
            this.Lvw.View = System.Windows.Forms.View.Details;
            // 
            // LblPagina
            // 
            this.LblPagina.AutoSize = true;
            this.TlpPrincipal.SetColumnSpan(this.LblPagina, 2);
            this.LblPagina.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LblPagina.Location = new System.Drawing.Point(5, 261);
            this.LblPagina.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.LblPagina.Name = "LblPagina";
            this.LblPagina.Size = new System.Drawing.Size(203, 25);
            this.LblPagina.TabIndex = 12;
            this.LblPagina.Text = "label2";
            // 
            // PagedListView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.TlpPrincipal);
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "PagedListView";
            this.Size = new System.Drawing.Size(417, 296);
            this.TlpPrincipal.ResumeLayout(false);
            this.TlpPrincipal.PerformLayout();
            this.FlpBotones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NudRegistrosPorPagina)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel TlpPrincipal;
        private System.Windows.Forms.Label LblRegistrosPorPagina;
        private System.Windows.Forms.Label LblTituloBusqueda;
        private System.Windows.Forms.TextBox TbxBusqueda;
        private System.Windows.Forms.NumericUpDown NudRegistrosPorPagina;
        private System.Windows.Forms.FlowLayoutPanel FlpBotones;
        private FontAwesome.Sharp.IconButton BtnLast;
        private FontAwesome.Sharp.IconButton BtnNext;
        private FontAwesome.Sharp.IconButton BtnPreviuos;
        private FontAwesome.Sharp.IconButton BtnFirst;
        private System.Windows.Forms.Label LblPagina;
        public CustomControls.ExtendedListView Lvw;
    }
}
